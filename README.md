# Is it Safe to Use a VPN on Your Mobile Device? #

One of the primary reasons to get a VPN is to increase your online security, so yes, usually a VPN is safe to use on mobile devices. However, there are some risks if you do not choose a good quality VPN.

Think of a VPN as an added layer of security every time you connect to the internet. Called a virtual private network, it connects you to a large encrypted network of computers. You then connect to the internet from a server on that network rather than from your mobile device. This increases your online privacy as your IP address and physical location are masked. It also makes it impossible for hackers to gain access to your device.   


## So is a Mobile VPN Safe? ##

Yes, but watch out for a few things when choosing a VPN to use. 

* Avoid free VPNs
* Don't use VPNs that log your online activity
* Stay away from VPNs that sell your data to advertising companies

Free VPNs need to make their revenue from somewhere and they will be. They would not just give you a free VPN for no reason. They may sell your browsing data to advertising companies or just interrupt your sessions with countless ads. Free VPNs are usually slower and have a capacity cap anyway, so your experience will not be good.

Also, when looking for a Mobile VPN check to see if they keep logs. This is not good at all. A VPN is supposed to be private, so pick one that actually is.

[https://personvernpanettet.no](https://personvernpanettet.no)
